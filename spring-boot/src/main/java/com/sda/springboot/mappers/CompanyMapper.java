package com.sda.springboot.mappers;

import com.sda.springboot.dtos.CompanyDto;
import com.sda.springboot.models.Company;
import com.sda.springboot.models.User;
import lombok.experimental.UtilityClass;

import java.util.List;
import java.util.stream.Collectors;

@UtilityClass
public class CompanyMapper {

    public CompanyDto toDto(Company company) {
        return CompanyDto.builder()
                .id(company.getId())
                .name(company.getName())
                .domain(company.getDomain())
                .users(UserMapper.toDto(company.getUsers()))
                .build();
    }

    public List<CompanyDto> toDto(List<Company> companies) {
        return companies.stream().map(CompanyMapper::toDto).collect(Collectors.toList());
    }

    public Company toModel(CompanyDto dto) {
        Company company = new Company();
        company.setName(dto.getName());
        company.setDomain(dto.getDomain());
        return company;
    }

    public Company toModel(CompanyDto dto, List<User> users) {
        Company company = toModel(dto);
        company.setUsers(users);
        return company;
    }

}
