package com.sda.springboot.mappers;

import com.sda.springboot.dtos.UserDto;
import com.sda.springboot.models.Company;
import com.sda.springboot.models.Role;
import com.sda.springboot.models.User;
import lombok.experimental.UtilityClass;

import java.util.List;
import java.util.stream.Collectors;

@UtilityClass
public class UserMapper {

    public UserDto toDto(User user) {
        UserDto dto = new UserDto();
        dto.setId(user.getId());
        dto.setName(user.getName());
        dto.setUsername(user.getUsername());
        dto.setEmail(user.getEmail());
        if (user.getCompany() != null) {
            dto.setCompanyId(user.getCompany().getId());
        }
        if (!user.getRoles().isEmpty()) {
            dto.setRoles(RoleMapper.toDto(user.getRoles()));
        }
        return dto;
    }

    public List<UserDto> toDto(List<User> users) {
        return users.stream().map(UserMapper::toDto).collect(Collectors.toList());
    }

    public User toModel(UserDto dto, List<Role> roles, Company company) {
        User user = new User();
        user.setName(dto.getName());
        user.setUsername(dto.getUsername());
        user.setPassword(dto.getPassword());
        user.setEmail(dto.getEmail());
        user.setRoles(roles);
        user.setCompany(company);
        return user;
    }
}
