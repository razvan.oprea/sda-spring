package com.sda.springboot.mappers;

import com.sda.springboot.dtos.RoleDto;
import com.sda.springboot.models.Role;
import lombok.experimental.UtilityClass;

import java.util.List;
import java.util.stream.Collectors;

@UtilityClass
public class RoleMapper {

    public RoleDto toDto(Role role) {
        return new RoleDto(role.getId(), role.getName());
    }

    public List<RoleDto> toDto(List<Role> roles) {
        return roles.stream().map(RoleMapper::toDto).collect(Collectors.toList());
    }

    public Role toModel(RoleDto dto) {
        Role role = new Role();
        role.setName(dto.getName());
        return role;
    }
}
