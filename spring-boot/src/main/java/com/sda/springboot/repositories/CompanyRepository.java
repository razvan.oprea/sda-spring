package com.sda.springboot.repositories;

import com.sda.springboot.models.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {
    boolean existsByName(String name);

    List<Company> findByDomainContainingIgnoreCase(String domain);
}
