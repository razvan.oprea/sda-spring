package com.sda.springboot.repositories;

import com.sda.springboot.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(String name);

    List<Role> findByNameContainingIgnoreCase(String name);

    void deleteByName(String name);

    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query("UPDATE Role role SET role.name = :name WHERE role.id = :id")
    void updateName(@Param("id") long id, @Param("name") String name);
}
