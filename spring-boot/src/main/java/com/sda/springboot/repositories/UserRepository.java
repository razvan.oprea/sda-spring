package com.sda.springboot.repositories;

import com.sda.springboot.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    List<User> findByName(String name);

    List<User> findByNameStartsWithIgnoreCase(String name);

    List<User> findByCompanyId(Long id);

    List<User> findByCompanyIdOrderByUsername(Long id);

    List<User> findByCompanyIdAndNameContainingIgnoreCase(Long id, String name);

    List<User> findByEmailEndsWithOrderByEmail(String email);

    Optional<User> findByEmail(String email);

    Optional<User> findByUsername(String username);

    Optional<User> findByUsernameOrEmail(String username, String email);

    boolean existsByNameContaining(String name);

    long countByEmailEndsWith(String email);
}
