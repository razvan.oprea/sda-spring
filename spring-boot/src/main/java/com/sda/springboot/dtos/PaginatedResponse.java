package com.sda.springboot.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PaginatedResponse<T> {
    private List<T> list;
    private Boolean hasNext;
    private Boolean hasPrevious;
    private Integer pages;
    private Long count;
}
