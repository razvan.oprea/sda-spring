package com.sda.springboot.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Builder
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CompanyDto {
    private final long id;
    @NotBlank
    private final String name;
    private final String domain;
    private final List<UserDto> users;
}
