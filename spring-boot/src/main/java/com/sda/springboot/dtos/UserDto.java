package com.sda.springboot.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto {

    private long id;

    @NotBlank
    @Size(min = 4, max = 32, message = "Name should be between 4 and 32 characters")
    private String name;

    @NotBlank
    private String username;

    @NotBlank
    private String password;

    @Email(message = "Email should be valid")
    private String email;

    private List<RoleDto> roles;

    private Long companyId;
}
