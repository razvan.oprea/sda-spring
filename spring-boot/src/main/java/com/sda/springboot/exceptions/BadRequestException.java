package com.sda.springboot.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends BaseAppException {
    private static final long serialVersionUID = -3387253215402827340L;

    public BadRequestException(String message) {
        super(message);
    }
}
