package com.sda.springboot.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class BaseAppException extends RuntimeException {
    private static final long serialVersionUID = -8751218704147400216L;

    public BaseAppException(String message) {
        super(message);
    }

    public HttpStatus getHttpStatus() {
        ResponseStatus annotation = this.getClass().getAnnotation(ResponseStatus.class);
        if (annotation != null) {
            return annotation.value();
        }
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }
}
