package com.sda.springboot.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends BaseAppException {
    private static final long serialVersionUID = 7125678760678400273L;

    public ResourceNotFoundException(String message) {
        super(message);
    }
}
