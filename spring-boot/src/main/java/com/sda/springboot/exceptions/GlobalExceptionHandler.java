package com.sda.springboot.exceptions;

import com.sda.springboot.dtos.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * This class will be responsible of intercepting all exceptions thrown in our application
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    /**
     * This method will intercept all our custom exceptions that extend BaseAppException.
     * Make sure the exception from exception handler is the same with the one as parameter.
     */
    @ExceptionHandler(BaseAppException.class)
    public final ResponseEntity<ErrorResponse> handleCustomExceptions(BaseAppException exception) {
        return new ResponseEntity<>(new ErrorResponse(exception.getMessage()), exception.getHttpStatus());
    }

    /**
     * This method will intercept all the other exceptions. Custom exceptions that do not extend BaseAppException
     * will be handled here as well.
     * Note: it is a bad practice/security vulnerability to expose the message from runtime exceptions in this method,
     * but it will help debugging our application during the exercises.
     */
    @ExceptionHandler(Exception.class)
    public final ResponseEntity<ErrorResponse> handleException(Exception exception) {
        return new ResponseEntity<>(new ErrorResponse("An unhandled exception of type: " +
                exception.getClass().getName() + " was thrown: " + exception.getMessage()),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
