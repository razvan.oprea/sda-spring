package com.sda.springboot.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class InternalServerException extends BaseAppException {
    private static final long serialVersionUID = -1213677292745780176L;

    public InternalServerException(String message) {
        super(message);
    }
}
