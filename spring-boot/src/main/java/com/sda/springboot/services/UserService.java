package com.sda.springboot.services;

import com.sda.springboot.dtos.UserDto;
import com.sda.springboot.exceptions.ResourceNotFoundException;
import com.sda.springboot.mappers.UserMapper;
import com.sda.springboot.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UserDto> findAll() {
        return userRepository.findAll().stream().map(UserMapper::toDto).collect(Collectors.toList());
    }

    public UserDto findById(Long id) {
        return userRepository.findById(id)
                .map(UserMapper::toDto)
                .orElseThrow(() -> new ResourceNotFoundException("User not found"));
    }

    public void delete(Long id) {
        userRepository.deleteById(id);
    }
}
