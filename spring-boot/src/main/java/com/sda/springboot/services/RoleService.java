package com.sda.springboot.services;

import com.sda.springboot.dtos.RoleDto;
import com.sda.springboot.exceptions.ResourceNotFoundException;
import com.sda.springboot.mappers.RoleMapper;
import com.sda.springboot.models.Role;
import com.sda.springboot.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class RoleService {

    private final RoleRepository roleRepository;

    @Autowired
    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public List<RoleDto> findAll() {
        return RoleMapper.toDto(roleRepository.findAll());
    }

    public RoleDto findById(Long id) {
        return roleRepository.findById(id)
                .map(RoleMapper::toDto)
                .orElseThrow(() -> new ResourceNotFoundException("Role with id " + id + " does not exist"));
    }

    public Long save(RoleDto dto) {
        Role role = roleRepository.save(RoleMapper.toModel(dto));
        return role.getId();
    }

    @Transactional
    public RoleDto update(Long id, RoleDto dto) {
        Role role = roleRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Role with id " + id + " does not exist"));
        role.setName(dto.getName());
        role = roleRepository.save(role);
        return RoleMapper.toDto(role);
    }

    public void delete(long id) {
        roleRepository.deleteById(id);
    }
}
