package com.sda.springboot.services;

import com.sda.springboot.dtos.CompanyDto;
import com.sda.springboot.dtos.PaginatedResponse;
import com.sda.springboot.dtos.UserDto;
import com.sda.springboot.exceptions.BadRequestException;
import com.sda.springboot.exceptions.ResourceNotFoundException;
import com.sda.springboot.mappers.CompanyMapper;
import com.sda.springboot.mappers.UserMapper;
import com.sda.springboot.models.Company;
import com.sda.springboot.models.Role;
import com.sda.springboot.models.User;
import com.sda.springboot.repositories.CompanyRepository;
import com.sda.springboot.repositories.RoleRepository;
import com.sda.springboot.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class CompanyService {

    private static final List<String> SORT_OPTIONS = Arrays.asList("id", "name", "domain");
    private final CompanyRepository companyRepository;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Autowired
    public CompanyService(CompanyRepository companyRepository, UserRepository userRepository, RoleRepository roleRepository) {
        this.companyRepository = companyRepository;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    public PaginatedResponse<CompanyDto> findAll(Integer page, Integer size, String sortBy) {
        if (!SORT_OPTIONS.contains(sortBy)) {
            throw new BadRequestException("Invalid sort parameter. Available options: " + SORT_OPTIONS);
        }
        List<Company> companies;
        PaginatedResponse<CompanyDto> response = new PaginatedResponse<>();
        if (page == null) {
            companies = companyRepository.findAll(Sort.by(sortBy));
        } else {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortBy));
            Page<Company> companyPage = companyRepository.findAll(pageable);
            companies = companyPage.getContent();
            response.setHasNext(companyPage.hasNext());
            response.setHasPrevious(companyPage.hasPrevious());
            response.setPages(companyPage.getTotalPages());
            response.setCount(companyPage.getTotalElements());
        }
        response.setList(CompanyMapper.toDto(companies));
        return response;
    }

    public CompanyDto findById(Long id) {
        return companyRepository.findById(id).map(CompanyMapper::toDto)
                .orElseThrow(() -> new ResourceNotFoundException("Company with id " + id + " does not exist"));
    }

    @Transactional
    public CompanyDto save(CompanyDto dto) {
        Company company = companyRepository.save(CompanyMapper.toModel(dto));
        return CompanyMapper.toDto(company);
    }

    @Transactional
    public CompanyDto update(Long id, CompanyDto dto) {
        Company company = getCompanyById(id);
        company.setName(dto.getName());
        company.setDomain(dto.getDomain());
        company = companyRepository.save(company);
        return CompanyMapper.toDto(company);
    }

    @Transactional
    public void delete(Long id) {
        companyRepository.deleteById(id);
    }

    /**
     * This method adds a user to a company, mapping that user to a default role. Another implementation would be adding the user to the roles received in the request.
     *
     * @param id      company id
     * @param userDto user to be created
     * @return company representation with the new added user
     */
    @Transactional
    public CompanyDto addUser(Long id, UserDto userDto) {
        Company company = getCompanyById(id);
        Role role = roleRepository.findByName("ROLE_USER")
                .orElseThrow(() -> new ResourceNotFoundException("Default role not found"));
        User user = UserMapper.toModel(userDto, Collections.singletonList(role), company);
        userRepository.saveAndFlush(user);

        return CompanyMapper.toDto(getCompanyById(id));
    }

    private Company getCompanyById(long id) {
        return companyRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Company with id " + id + " does not exist"));
    }
}
