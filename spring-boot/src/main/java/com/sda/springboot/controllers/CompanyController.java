package com.sda.springboot.controllers;

import com.sda.springboot.dtos.CompanyDto;
import com.sda.springboot.dtos.PaginatedResponse;
import com.sda.springboot.dtos.UserDto;
import com.sda.springboot.services.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class CompanyController extends BaseController {

    public static final String API_NAME = "companies";

    private final CompanyService companyService;

    @Autowired
    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping(CompanyController.API_NAME)
    public ResponseEntity<PaginatedResponse<CompanyDto>> findAll(@RequestParam(name = "page", required = false) Integer page,
                                                                 @RequestParam(name = "size", defaultValue = "10") Integer size,
                                                                 @RequestParam(name = "sortBy", defaultValue = "id") String sortBy) {
        return new ResponseEntity<>(companyService.findAll(page, size, sortBy), HttpStatus.OK);
    }

    @GetMapping(CompanyController.API_NAME + "/{id}")
    public ResponseEntity<CompanyDto> findById(@PathVariable Long id) {
        return new ResponseEntity<>(companyService.findById(id), HttpStatus.OK);
    }

    @PostMapping(CompanyController.API_NAME)
    public ResponseEntity<CompanyDto> save(@Valid @RequestBody CompanyDto dto) {
        return new ResponseEntity<>(companyService.save(dto), HttpStatus.CREATED);
    }

    @PutMapping(CompanyController.API_NAME + "/{id}")
    public ResponseEntity<CompanyDto> update(@PathVariable Long id, @Valid @RequestBody CompanyDto dto) {
        return new ResponseEntity<>(companyService.update(id, dto), HttpStatus.OK);
    }

    @DeleteMapping(CompanyController.API_NAME + "/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        companyService.delete(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }

    @PostMapping(CompanyController.API_NAME + "/{id}/users")
    public ResponseEntity<CompanyDto> addUser(@PathVariable Long id, @Valid @RequestBody UserDto dto) {
        return new ResponseEntity<>(companyService.addUser(id, dto), HttpStatus.CREATED);
    }
}
