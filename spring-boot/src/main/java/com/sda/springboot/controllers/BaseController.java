package com.sda.springboot.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(BaseController.API_V1)
public abstract class BaseController {
    protected static final String API_V1 = "/api/v1";
}
