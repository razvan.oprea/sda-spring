package com.sda.springboot.controllers;

import com.sda.springboot.exceptions.BadRequestException;
import com.sda.springboot.exceptions.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("exceptions")
public class TestExceptionsController {

    @GetMapping("/bad-request")
    public ResponseEntity<?> testBadRequestException() {
        throw new BadRequestException("Bad request exception thrown!");
    }

    @GetMapping("/not-found")
    public ResponseEntity<?> testResourceNotFoundException() {
        throw new ResourceNotFoundException("Resource not found exception thrown!");
    }

    @GetMapping("/exception")
    public ResponseEntity<?> testException() throws Exception {
        throw new Exception("Exception thrown!");
    }

    @GetMapping("/runtime")
    public ResponseEntity<?> testRuntimeException() throws Exception {
        throw new NullPointerException("Null pointer exception thrown!");
    }
}
