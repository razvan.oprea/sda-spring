package com.sda.springboot.controllers;

import com.sda.springboot.dtos.RoleDto;
import com.sda.springboot.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class RoleController extends BaseController {

    public static final String API_NAME = "roles";

    private final RoleService roleService;

    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @GetMapping(RoleController.API_NAME)
    public ResponseEntity<List<RoleDto>> findAll() {
        return ResponseEntity.ok(roleService.findAll());
    }

    @GetMapping(RoleController.API_NAME + "/{id}")
    public ResponseEntity<RoleDto> findById(@PathVariable Long id) {
        return ResponseEntity.ok(roleService.findById(id));
    }

    @PostMapping(RoleController.API_NAME)
    public ResponseEntity<Long> save(@Valid @RequestBody RoleDto dto) {
        return new ResponseEntity<>(roleService.save(dto), HttpStatus.CREATED);
    }

    @PutMapping(RoleController.API_NAME + "/{id}")
    public ResponseEntity<RoleDto> update(@PathVariable Long id, @Valid @RequestBody RoleDto dto) {
        return new ResponseEntity<>(roleService.update(id, dto), HttpStatus.OK);
    }

    @DeleteMapping(RoleController.API_NAME + "/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        roleService.delete(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }
}
