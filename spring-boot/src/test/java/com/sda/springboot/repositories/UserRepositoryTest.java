package com.sda.springboot.repositories;

import com.sda.springboot.models.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;


@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testFindByNameExisting() {
        String existingName = "Anna Microsoft";

        List<User> users = userRepository.findByName(existingName);

        // Using junit assertions
        assertEquals(1, users.size());
        assertEquals(existingName, users.get(0).getName());

        // Using assertJ assertions
        assertThat(users.size()).isEqualTo(1);
        assertThat(users.get(0).getName()).isEqualTo(existingName);
    }

    @Test
    public void testFindByNameInvalidName() {
        String invalidName = "Some name";

        List<User> users = userRepository.findByName(invalidName);

        assertThat(users).isEmpty();
    }


    @Test
    public void testFindByNameStartsWithIgnoreCase() {
        String namePrefix = "ja";

        List<User> users = userRepository.findByNameStartsWithIgnoreCase(namePrefix);

        assertThat(users).hasSize(2);
        users.forEach(user -> assertThat(user.getName().toLowerCase()).startsWith(namePrefix));
    }

    @Test
    public void testFindByEmailEndsWithOrderByEmail() {
        String emailSuffix = "@microsoft.com";

        List<User> users = userRepository.findByEmailEndsWithOrderByEmail(emailSuffix);
        List<User> sortedUsersByEmail = users.stream()
                .sorted(Comparator.comparing(User::getEmail))
                .collect(Collectors.toList());

        assertThat(users).hasSize(4);
        assertThat(users).isEqualTo(sortedUsersByEmail);
        users.forEach(user -> assertThat(user.getEmail().toLowerCase()).endsWith(emailSuffix));
    }

    @Test
    public void testFindByUsernameOrEmail() {
        String username = "john.microsoft";
        String email = "john@microsoft.com";

        Optional<User> validUser = userRepository.findByEmail(email);

        assertEquals(validUser, userRepository.findByUsernameOrEmail(username, email));
        assertEquals(validUser, userRepository.findByUsernameOrEmail(username, "invalid email"));
        assertEquals(validUser, userRepository.findByUsernameOrEmail(username, null));
        assertEquals(validUser, userRepository.findByUsernameOrEmail("invalid username", email));
        assertEquals(validUser, userRepository.findByUsernameOrEmail(null, email));

        assertEquals(Optional.empty(), userRepository.findByUsernameOrEmail("wrong", null));
    }
}