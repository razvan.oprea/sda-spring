package com.sda.springboot.repositories;

import com.sda.springboot.models.Role;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class RoleRepositoryTest {

    @Autowired
    private RoleRepository roleRepository;

    @Test
    public void testDeleteByName() {
        Role role = new Role();
        role.setName("TEST_ROLE");
        roleRepository.save(role);
        long initialCount = roleRepository.count();
        roleRepository.deleteByName(role.getName());

        assertEquals(roleRepository.count(), initialCount - 1);
    }

    @Test
    public void testDeleteByNameShouldThrowExceptionWhenUserAssignedToIt() {
        Role role = roleRepository.findById(1L).orElseThrow(() ->
                new RuntimeException("Role with id 1 not found"));

        assertThrows(DataIntegrityViolationException.class, () -> {
            roleRepository.deleteByName(role.getName());
            roleRepository.flush();
        });
    }

    @Test
    public void testDeleteByNameNotExisting() {
        long initialCount = roleRepository.count();
        roleRepository.deleteByName("INVALID_NAME");

        assertEquals(roleRepository.count(), initialCount);
    }

    @Test
    public void testUpdateName() {
        String testRole = "ROLE_TEST";
        Role role = roleRepository.findById(1L).orElseThrow(() ->
                new RuntimeException("Role with id 1 not found"));
        roleRepository.updateName(role.getId(), testRole);

        role = roleRepository.findById(1L).orElseThrow(() ->
                new RuntimeException("Role with id 1 not found"));
        assertEquals(testRole, role.getName());
    }

    @Test
    public void testFindAllPaginatedAndSorted() {
        List<Role> expectedList = roleRepository.findAll().stream()
                .sorted(Comparator.comparing(Role::getName))
                .limit(2)
                .collect(Collectors.toList());
        Pageable firstPageWithTwoElementsSortedByName = PageRequest.of(0,2,Sort.by("name"));
        Page<Role> page = roleRepository.findAll(firstPageWithTwoElementsSortedByName);

        assertEquals(expectedList, page.getContent());
    }
}