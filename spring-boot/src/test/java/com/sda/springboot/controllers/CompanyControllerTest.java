package com.sda.springboot.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@SpringBootTest
@Transactional
public class CompanyControllerTest {

    private static final String BASE_URI = CompanyController.API_V1 + "/" + CompanyController.API_NAME;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetAllCompanies() throws Exception {
        mockMvc.perform(get(BASE_URI).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.list", hasSize(3)))
                .andExpect(jsonPath("$.list[0].id").value("1"))
                .andExpect(jsonPath("$.list[0].name").value("Microsoft"))
                .andExpect(jsonPath("$.list[0].domain").value("IT"));
    }

    @Test
    public void testDeleteCompany() throws Exception {
        mockMvc.perform(delete(BASE_URI + "/1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted())
                .andExpect(jsonPath("$").doesNotExist());
    }
}