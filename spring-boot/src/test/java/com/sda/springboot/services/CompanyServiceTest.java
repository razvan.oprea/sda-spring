package com.sda.springboot.services;


import com.sda.springboot.dtos.CompanyDto;
import com.sda.springboot.dtos.PaginatedResponse;
import com.sda.springboot.exceptions.BadRequestException;
import com.sda.springboot.exceptions.ResourceNotFoundException;
import com.sda.springboot.models.Company;
import com.sda.springboot.repositories.CompanyRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.Optional;

import static com.sda.springboot.MockFactory.createCompany;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CompanyServiceTest {

    @Mock
    private CompanyRepository companyRepository;

    @InjectMocks
    private CompanyService companyService;

    @Test
    public void testFindById() {
        when(companyRepository.findById(1L)).thenReturn(Optional.of(createCompany(1L, "Name", "Domain")));
        when(companyRepository.findById(2L)).thenReturn(Optional.empty());

        CompanyDto result = companyService.findById(1L);
        assertThat(result).isNotNull();
        assertThat(result.getId()).isEqualTo(1L);
        assertThat(result.getName()).isEqualTo("Name");
        assertThat(result.getDomain()).isEqualTo("Domain");

        assertThrows(ResourceNotFoundException.class, () -> companyService.findById(2L));
    }

    @Test
    public void testFindAllWithoutPagination() {
        when(companyRepository.findAll(any(Sort.class))).thenReturn(Arrays.asList(
                createCompany(1L, "Name", "Domain"),
                createCompany(2L, "Name2", null),
                createCompany(3L, "Name3", "Domain3")));

        PaginatedResponse<CompanyDto> result = companyService.findAll(null, null, "id");
        assertThat(result).isNotNull();
        assertThat(result.getList()).hasSize(3);
        assertThat(result.getList().get(0).getId()).isEqualTo(1);
        assertThat(result.getList().get(1).getDomain()).isNull();
        assertThat(result.getCount()).isNull();
        assertThat(result.getHasNext()).isNull();
        assertThat(result.getHasPrevious()).isNull();
        assertThat(result.getPages()).isNull();
    }

    @Test
    public void testFindAllInvalidSortParameter() {
        assertThrows(BadRequestException.class, () -> companyService.findAll(null, null, "invalidParam"));
        assertThrows(BadRequestException.class, () -> companyService.findAll(1, 10, "invalidParam"));
        assertThrows(BadRequestException.class, () -> companyService.findAll(1, 10, null));
    }

    @Test
    public void testFindAllWithPagination() {
        Page<Company> page = new PageImpl<>(Arrays.asList(
                createCompany(1L, "Name", "Domain"),
                createCompany(2L, "Name2", null),
                createCompany(3L, "Name3", "Domain3"),
                createCompany(4L, "Name4", "Domain4"),
                createCompany(5L, "Name5", "Domain5")));
        when(companyRepository.findAll(any(Pageable.class))).thenReturn(page);

        PaginatedResponse<CompanyDto> result = companyService.findAll(0, 5, "id");
        assertThat(result).isNotNull();
        assertThat(result.getList()).hasSize(5);
        assertThat(result.getCount()).isEqualTo(5);
    }
}