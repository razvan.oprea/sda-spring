package com.sda.beans;

import com.sda.interfaces.Writer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileWriter implements Writer {
    private String filePath;

    public FileWriter(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public void write(String text) {
        try {
            System.out.println("[File] Writing '" + text + "' into " + filePath);
            Files.write(Paths.get("spring-core/src/main/resources",filePath), text.getBytes());
        } catch (IOException e) {
            System.out.println("Could not write " + text + " to path " + filePath + ": " + e.toString());
        }
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
