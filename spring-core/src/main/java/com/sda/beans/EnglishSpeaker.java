package com.sda.beans;

import com.sda.interfaces.Speaker;
import com.sda.interfaces.Writer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;

@Primary
@Component
public class EnglishSpeaker implements Speaker {

    private final Writer writer;
    private final List<String> words;
    private final RandomNumber randomNumber;

    @Autowired
    public EnglishSpeaker(@Qualifier("consoleWriter") Writer writer, @Qualifier("generatedWords") List<String> words, RandomNumber randomNumber) {
        System.out.println("Constructor method in EnglishSpeaker");
        this.writer = writer;
        this.words = words;
        this.randomNumber = randomNumber;
    }

    @Override
    public void sayHello() {
        writer.write("English speaker - Hello!");
    }

    @Override
    public void sayRandomWord() {
        String randomWord = words.get(randomNumber.getValue());
        writer.write("English speaker - random word: " + randomWord);
    }

    @PostConstruct
    private void postConstruct() {
//        System.out.println("Calling postConstruct() method in EnglishSpeaker");
    }

    @PreDestroy
    private void preDestroy() {
//        System.out.println("Calling preDestroy() method in EnglishSpeaker");
    }
}
