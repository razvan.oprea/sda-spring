package com.sda.beans;

import com.sda.interfaces.Speaker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Person {
    private final List<Speaker> speakers;

    public Person(Speaker... speakers) {
        this.speakers = new ArrayList<>(Arrays.asList(speakers));
    }

    public Person(List<Speaker> speakers) {
        this.speakers = speakers;
    }

    public void saySomething() {
        speakers.forEach(Speaker::sayHello);
    }
}
