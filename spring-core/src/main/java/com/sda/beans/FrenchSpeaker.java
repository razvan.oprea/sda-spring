package com.sda.beans;

import com.sda.interfaces.Speaker;
import com.sda.interfaces.Writer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FrenchSpeaker implements Speaker {

    private final Writer writer;
    private final List<String> words;
    private final RandomNumber randomNumber;

    @Autowired
    public FrenchSpeaker(@Qualifier("fileWriter") Writer writer, @Qualifier("generatedWords") List<String> words, RandomNumber randomNumber) {
        this.writer = writer;
        this.words = words;
        this.randomNumber = randomNumber;
    }

    @Override
    public void sayHello() {
        writer.write("French speaker - Hello!");
    }

    @Override
    public void sayRandomWord() {
        String randomWord = words.get(randomNumber.getValue());
        writer.write("French speaker - random word: " + randomWord);
    }
}
