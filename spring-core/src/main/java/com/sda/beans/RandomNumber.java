package com.sda.beans;

public class RandomNumber {
    private final int value;

    public RandomNumber(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
