package com.sda.beans;

import com.sda.interfaces.Writer;


public class ConsoleWriter implements Writer {
    @Override
    public void write(String text) {
        System.out.println("[Console] " + text);
    }
}
