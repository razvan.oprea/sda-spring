package com.sda.interfaces;

public interface Writer {
    void write(String text);
}
