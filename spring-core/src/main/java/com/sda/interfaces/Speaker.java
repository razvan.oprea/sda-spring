package com.sda.interfaces;

public interface Speaker {
    void sayHello();
    void sayRandomWord();
}
