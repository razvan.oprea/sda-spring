package com.sda;

import com.sda.beans.*;
import com.sda.config.AppConfig;
import com.sda.interfaces.Speaker;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpringApp {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(AppConfig.class);
        context.refresh();

        // Ex2 - get speaker beans from context
        System.out.println("\n* Exercise 1 - get speaker beans and say hello");
        Speaker englishSpeaker = context.getBean(EnglishSpeaker.class);
        Speaker frenchSpeaker = (FrenchSpeaker) context.getBean("frenchSpeaker");
        englishSpeaker.sayHello();
        frenchSpeaker.sayHello();

        // Ex3 - primary bean
        System.out.println("\n* Exercise 2 - see primary bean");
        Speaker speaker = context.getBean(Speaker.class);
        speaker.sayHello();

        // Ex4 - random numbers
        System.out.println("\n* Exercise 4 - random numbers");
        RandomNumber number1 = context.getBean(RandomNumber.class);
        RandomNumber number2 = context.getBean(RandomNumber.class);
        System.out.println("Number1 equals Number2: " + number1.equals(number2));
        System.out.println("Number1 value: " + number1.getValue());
        System.out.println("Number2 value: " + number2.getValue());


        // Ex8 - write to a different file
        FileWriter fileWriter = context.getBean(FileWriter.class);
        fileWriter.setFilePath("file2.txt");
        frenchSpeaker.sayHello();

        // Ex9 - say random word
        System.out.println("\n* Exercise 9 - say random word");
        englishSpeaker.sayRandomWord();
        fileWriter.setFilePath("random.txt");
        frenchSpeaker.sayRandomWord();

        // Ex10 - create a person
        System.out.println("\n* Exercise 10 - create persons");
        Person englishPerson = (Person) context.getBean("englishPerson");
        Person frenchPerson = (Person) context.getBean("frenchPerson");
        Person bilingualPerson = (Person) context.getBean("bilingualPerson");
        englishPerson.saySomething();
        fileWriter.setFilePath("french.txt");
        frenchPerson.saySomething();
        fileWriter.setFilePath("bilingual.txt");
        bilingualPerson.saySomething();

        context.close();
    }
}
