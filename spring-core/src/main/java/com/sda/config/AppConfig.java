package com.sda.config;

import com.sda.beans.ConsoleWriter;
import com.sda.beans.FileWriter;
import com.sda.beans.Person;
import com.sda.beans.RandomNumber;
import com.sda.interfaces.Speaker;
import com.sda.interfaces.Writer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.*;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Configuration
@ComponentScan("com.sda")
public class AppConfig {

    private static final String DEFAULT_PATH = "file.txt";

    @Autowired
    private List<Speaker> speakers;

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public RandomNumber randomNumber() {
        int randomNumber = (int) (Math.random() * 100);
        return new RandomNumber(randomNumber);
    }

    @Bean
    @Primary
    public Writer consoleWriter() {
        return new ConsoleWriter();
    }

    @Bean
    public Writer fileWriter() {
        return new FileWriter(DEFAULT_PATH);
    }

    @Bean(name = "generatedWords")
    public List<String> words() {
        // This is just a fancy method to generate 100 Strings: Word0, Word1, ... Word99
        AtomicInteger integer = new AtomicInteger(0);
        return Stream.generate(() -> "Word" + integer.getAndIncrement())
                .limit(100)
                .collect(Collectors.toList());
    }

    @Bean
    public Person englishPerson() {
        return new Person(speakers.get(0));
    }

    @Bean
    public Person frenchPerson() {
        return new Person(speakers.get(1));
    }

    @Bean
    public Person bilingualPerson() {
        return new Person(speakers);
    }

}
